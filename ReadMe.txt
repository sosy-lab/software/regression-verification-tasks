The directory 'regression-verification-tasks' contains verification tasks 
that can be used to evaluate approaches for regression checking.

Documentation of this collection of verification tasks:
http://www.sosy-lab.org/~dbeyer/cpa-reuse/regression-benchmarks/

A research project in which the verification tasks were used:
http://www.sosy-lab.org/~dbeyer/cpa-reuse/

